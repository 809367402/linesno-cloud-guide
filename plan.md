## 开发项目集成平台开发指导

### 愿景
- 开发人员根据指导文档，以最小成本（时间/人力) 搭建第一个示例项目
- 开发人员熟悉集成过程，并快速投入业务研发

### 指引集成进度

| 序号 | 计划说明             | 维护人员 | 状态   | 备注 |
|------|----------------------|----------|--------|------|
| 1    | 基线介绍             | 研发     | 集成中 |      |
| 2    | 执行计划             | 研发     |        |      |
| 3    | 配置私服             | 研发     |        |      |
| 4    | 开始一个职工管理项目 | 研发     | 集成中 |      |
| 5    | 第一个web工程        | 研发     | 集成中 |      |
| 6    | 第一个服务工程       | 研发     |        |      |
| 7    | 引入其它组件         | 研发     |        |      |
| 8    | 完成第一天的任务     | 研发     |        | .    |
